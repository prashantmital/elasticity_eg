import matplotlib.pyplot as plt
import numpy as np

with open("datei_functionals_miehe_tension_ref_adapt_4_1.txt", 'r') as f_read:
    raw_refdata = f_read.readlines()
    refdata = np.asarray([[float(x[3])for x in [data.split() for data in raw_refdata]],
            [float(x[12])for x in [data.split() for data in raw_refdata]]], 'float64')

with open("screenout_miehe.out", 'r') as f_read:
    _testdata_uy = list()
    _testdata_Fy = list()
    for lines in f_read:
        try:
            line = lines.split()
            if (line[0] == "Fy:"):
                _testdata_uy.append(float(line[1])) 
                _testdata_Fy.append(float(line[2]))
        except IndexError:
            continue

    testdata = np.asarray([_testdata_uy,_testdata_Fy], 'float64')

def plot_miehe(refdata, testdata):
    p1, = plt.plot(refdata[0, :], refdata[1, :], 'b-', label="Reference Data", linewidth=1)
    p1, = plt.plot(testdata[0, :], testdata[1, :], 'g-', label="Test Data", linewidth=1)
    plt.legend(loc=2)
    plt.xlabel("Displacement u_y [mm]")
    plt.ylabel("Force F_y [N]")
    plt.title("Miehe Tension Test Comparsion with reference curve")
    plt.grid(True)
    plt.show()
    print "END"
    return p1


plot_miehe(refdata, testdata)
