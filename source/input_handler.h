/*
 * parameter_input.h
 *
 *  Created on: Apr 28, 2015
 *      Author: mital
 */
#ifndef INPUT_HANDLER_H_
#define INPUT_HANDLER_H_

#include <deal.II/base/parameter_handler.h>
#include <list>
#include <iostream>
#include <fstream>

using namespace dealii;

class InputHandler {
public:
	InputHandler (const int argc, char *const *argv) :
		flag_success(false),
		sform (1),
	    max_iters_newt(10),
        penalty_DG(1),
        penalty_BC(10),
        lame_mu(81),
        lame_lambda(122),
	    tolerance_newton (1.0e-8)
    {
		//Declare and Populate
		declare_parameters();
		parse_command_line(argc, argv);
    };

	int get_sform () const { return sform; }
    int get_globalrefines () const { return global_refines; } // n global refines
    int get_maxitersnewt () const { return max_iters_newt; }
	double get_penaltyDG () const { return penalty_DG; } // penalty_u_face
	double get_penaltyBC () const { return penalty_BC; } // penalty_u_bc
    double get_lamemu () const { return lame_mu; } // lame_coefficient_mu
    double get_lamelambda () const { return lame_lambda; } // lame_coefficient_lambda
    double get_tolerancenewton () const { return tolerance_newton; } 
    std::string get_testcase () const { return testcase; } // mesh input filename
    //void echo_parameters () const;

private:
	void print_usage_message ();
	void declare_parameters ();
	void parse_command_line (const int argc, char *const *argv);

	ParameterHandler prm;
	bool flag_success;
	int sform, global_refines, max_iters_newt;
	double penalty_DG, penalty_BC, lame_mu, lame_lambda, tolerance_newton;
    std::string testcase;
};

#endif
