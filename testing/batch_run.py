"""
Script to generate the parameter files and
run the deal.II jobs
"""

import shutil
import os
from itertools import product
from numpy import ceil
from datetime import datetime
from subprocess import call
from sys import argv

def get_template(fname):
    '''
    Read the template data from file called fname
    '''
    with open(fname, 'r') as fh:
        data = fh.readlines()
    return data

def merge_dicts(*dict_args):
    '''
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    '''
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

def get_prm_format():
    prm_key = {
            "boundary_p":"set Boundary Term Penalty",
            "face_p":"set Face Term Penalty",
            "sform":"set sform",
            "mu":"set Lame's Parameter Mu",
            "lambda":"set Lame's Parameter Lambda",
            "refines":"set Num Global Refines",
            "testcase":"set Test Case",
            "NWtolerance":"set Newton Tolerance",
            }
    return prm_key



def generate_prm(job, basefile):
    '''
    Generate prm file contents while changing some parameters
    '''
    prm_format = get_prm_format()
    for index in range(len(basefile)):
        for key in job:
            if basefile[index].find(prm_format[key]) != -1:
                basefile[index] = (prm_format[key]
                        + " = " + str(job[key]) + "\n")
            else:
                continue
    
    return basefile


if __name__=='__main__':
    # Generate job timestamp
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    # Initialize log summary
    log = list()

    call('rm batch_screenout.out', shell=True)

#    lambda_list = [pow(2,x) for x in xrange(11)]
#    for k in lambda_list:
#        job = {"lambda":k}
#        with open("batch_base.prm", 'r') as f1:
#            basefile = f1.readlines()
#        jobfile = generate_prm(job, basefile)
#        with open("batch_test.prm", 'w') as f1:
#            f1.writelines(jobfile)
#        call('./step-30 -p batch_test.prm | tee -a batch_screenout.out', shell=True)
    
    for k in xrange(3, 7):
        job = {"refines":k}
        with open("batch_base.prm", 'r') as f1:
            basefile = f1.readlines()
        jobfile = generate_prm(job, basefile)
        with open("batch_test.prm", 'w') as f1:
            f1.writelines(jobfile)
        call('./step-30 -p batch_test.prm | tee -a batch_screenout.out', shell=True)

    print "DONE"
