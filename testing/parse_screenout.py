import numpy as np
from sys import argv
import matplotlib.pyplot as plt

def read_screenout(filename):
    '''
    reads data from specified file and 
    returns a list of lines
    '''
    with open(filename, 'r') as f1:
        raw_data = f1.readlines()
    return raw_data

def define_data_organization():
    '''
    define a dictionary that 
    specifies three items (given in order here):
    1. Column number in which this qty will be stored in the final array
    2. Keyword to search for to find this qty
    3. Position of the qty if the string is split on whitespace
    '''
    dataorg = {
            "ncells":[0, "Cells", 1],
            "L2err":[1, "L2 Norm of Error", 4],
            "stdH1err":[2, "Standard H1", 5],
            "emwH1err":[3, "Element-wise", 5],
            "massbalLinf":[4, "L-inf norm", 6],
            "massbalL2":[5, "L2 norm of local", 6],
            "stresscell":[6, "Interior", 3],
            "stressface":[7, "Face", 3]
            }
    return dataorg

def parse_rawdata(rawdata):
    '''
    performs the bulk of the comprehension
    '''
    #define here the size of one program run in num of lines
    dataorg = define_data_organization()
    #lines_per_step = 29
    lines_per_step = 31
    n_rows = int(len(rawdata)/lines_per_step)
    n_cols = len(dataorg.keys())
    parsed_data = np.ndarray([n_rows, n_cols], dtype=float) 
    
    for index in range(len(rawdata)):
        for key in dataorg:
            if rawdata[index].find(dataorg[key][1]) != -1:
                value_pos = dataorg[key][2]
                value = float(rawdata[index].split()[value_pos])
                row_num = int(np.floor(index/lines_per_step))
                col_num = dataorg[key][0]
                parsed_data[row_num, col_num] = value
            else:
                pass

    return parsed_data;


def serialize_parsed(parsed_data, targetfile):
    '''
    Uses a numpy routine to serialize the data so that it can be later read by the plotting scripts
    '''
    print parsed_data
    with open(targetfile, 'w') as file1:    
        np.save(file1, parsed_data)




if __name__=='__main__':
    scriptname, datafile, targetfile = argv
    serialize_parsed(parse_rawdata(read_screenout(datafile)), targetfile)

